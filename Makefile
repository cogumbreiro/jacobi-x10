SOURCE  = \
	x10/JacobiCF.x10 \
	x10/JacobiSeq.x10 \
	x10/JacobiSPMD.x10 \
	x10/JacobiOMP.x10 \
	x10/JacobiSPMD_Barrier.x10 \
	x10/Jacobi.x10 \
	x10/OMPBarrier.x10

ARMUS_JAR = armus-x10/target/armusc-x10.jar

ARMUSC = java -jar $(ARMUS_JAR)

X10C = x10c

ORIG = origin
CHECK = checked

all: init

init: $(ARMUS_JAR) origin.jar checked.jar stats-tk benchmarktk \
	data/$(ORIG) data/$(CHECK) logs/$(ORIG) logs/$(CHECK)

origin.jar: $(SOURCE)
	$(X10C) -o $@ x10/*.x10

checked.jar: origin.jar
	$(ARMUSC) $< $@

armus-x10:
	(git clone https://bitbucket.org/cogumbreiro/armus-x10)

$(ARMUS_JAR): armus-x10
	(cd armus-x10; git pull; ant)

run: init
	./benchmarktk/run-benchmark setup.yaml

collect: init
	./benchmarktk/collect-data setup.yaml

benchmarktk:
	git clone https://bitbucket.org/cogumbreiro/benchmarktk/

stats-tk:
	git clone https://bitbucket.org/cogumbreiro/stats-tk/ stats-tk

data/$(ORIG):
	mkdir -p $@

data/$(CHECK):
	mkdir -p $@

logs/$(ORIG):
	mkdir -p $@

logs/$(CHECK):
	mkdir -p $@


